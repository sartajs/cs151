Your name: Sartaj Singh
Your student ID: 010342670
Your major: Computer Science
Your preferred email address: ssingh.sartaj@gmail.com
Check one: [x] Undergraduate, [ ] Graduate, [ ] Open university
Graduating senior? [ ] Yes, [x] No
Repeating the class? [ ] Yes, [x] No
Place and semester where you took a programming course in Java: DeAnza, 2018 Winter 
Course number and grade received: D036B, A+
Place and semester where you took a course in data structures: SJSU Fall 2018 
Course number and grade received: CS146, B-
Time it took you to answer the two questions below:
Did you ask anyone for help? [ ] Yes, [x] No
Preferred section (if adding): [ ] 3, [ ] 4, [ ] 6
Other acceptable sections (if adding):