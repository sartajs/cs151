import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
/**
 * class that creates a vertical edge followed by a horizontal edge
 * @author Taj
 *
 */
public class VHEdge extends LineEdge
{
   /**
    * overrides draw method so it draws a vertical line towards the point then a horizontal between the connection points
    * @param g2 the Graphics2D object
    */
   public void draw(Graphics2D g2)
   {
      Line2D tempLine = getConnectionPoints();
      Point2D tempV = new Point2D.Double(tempLine.getX1() ,tempLine.getY2());
      Line2D vertical = new Line2D.Double(tempLine.getP1(), tempV);
      Line2D horizontal = new Line2D.Double(vertical.getP2(),tempLine.getP2());
      g2.draw(vertical);
      g2.draw(horizontal);
   }
}
