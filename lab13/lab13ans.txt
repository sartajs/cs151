lab16ans.txt

Lab Partner: Cagan Sevencan
1.) When we point the browser we get an error:

 "Action not found"
 For request 'GET /select/day13q1/C'


 2.) We added the following text to routes:

 # question = day13q1 ; answer = c ; From : For request 'GET /select/day13q1/C'
GET  /select/:question/:answer controllers.HomeController.select(question : String, answer: String)

3.) The output we now get is:

You selected C for day13q1

4.) If the problem does not exist, it adds the problem to the Hashmap. If the choice does not exist, it adds the choice to the problem hashmap. Furthermore, if the choice exists already, it adds to the number of choices made (+1).

5.) Our method is:

public Result view(String problem){
	return ok("" + (results.get(problem)));
}

6.) The answers are incremented C - twice, B - once, and A - once.
Then, view shows all the counters for C, B, and A.



