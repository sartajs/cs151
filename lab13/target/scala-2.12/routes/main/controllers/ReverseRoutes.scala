// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/Taj/cs151/lab13/conf/routes
// @DATE:Tue Mar 19 16:00:03 PDT 2019

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def select(question:String, answer:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "select/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("question", question)) + "/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("answer", answer)))
    }
  
    // @LINE:15
    def view(question:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "view/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("question", question)))
    }
  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:9
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }


}
