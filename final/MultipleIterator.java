import java.util.*;
public class MultipleIterator implements Iterator{

	List<Iterator> iList = new ArrayList();
	private int curr;
	
	public MultipleIterator(List<Iterator> iList) {
		curr = 0;
		this.iList = iList;
	}
	
	public String next() {
		
			Iterator temp = iList.get(curr);
			if(temp.hasNext()) {
				return ""+temp.next();	
			}
			else {
				curr++;
			}
			if(!this.hasNext()) {
				throw new NoSuchElementException();
			}
			this.next();
			return null;
			
		}

	@Override
	public boolean hasNext() {
		if(curr == iList.size()) {
			return false;
		}
		else 
			return true;
	}
	
	public void remove() {
		throw new UnsupportedOperationException();
	}
	}
	
