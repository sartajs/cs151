'use strict'
const { findNode } = require('./graph')
const n1 = { x: 5, y: 5, color: 'white', size: 5}
const n2 = { x: 10, y: 10, color: 'black', size: 5}

const e = { start: n1, end: n2}

const graph = { nodes: [n1, n2], edges :[e], type : 'simple'}

test('there is a white node at 5,5 and a black node at 10,10', () => {
	expect (findNode(graph,5,5).color).toBe('white')
	})