import java.util.ArrayList;

public class Lists
{
   public static void swapLargestAndSecondSmallest(ArrayList<Integer> a)
   {
       int largest = 0;
	      int small = -1;
	      int next = -1;

		   for(int i = 0; i<a.size(); i++){
			   if(a.get(i)>a.get(largest)){
				   largest = i;
			   }
					if(small==-1)
						small = i;
					
					else if(a.get(i)<a.get(small)) {
						next = small;
						small = i;
					}
					else if( next==-1 || a.get(i)<a.get(next) && a.get(i)>a.get(small))
						next = i;
				   
		   }
		   
		   if(next>0) {
			   int temp = a.get(largest);
			   a.set(largest, a.get(next));
			   a.set(next, temp);
			   
		   }
	
   }
}