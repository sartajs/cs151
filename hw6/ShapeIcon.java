import java.awt.*;
import java.util.*;
import javax.swing.*;

/**
   An icon that contains a moveable shape.
 */
public class ShapeIcon implements Icon
{
	/**
	 * creates ShapeIcon with given MoveableShape, width, and height
	 * @param shape the shape for the icon to be created from
	 * @param width width of the icon
	 * @param height height of the icon
	 */
	public ShapeIcon(MoveableShape shape,
			int width, int height)
	{
		this.shape = shape;
		this.width = width;
		this.height = height;
	}
	/**
	 * getter for width
	 * @return width the width of shapeIcon
	 */
	public int getIconWidth()
	{
		return width;
	}
	/**
	 * getter for height
	 * @return width the height of shapeIcon
	 */
	public int getIconHeight()
	{
		return height;
	}
	/**
	 * draws the given icon
	 * @param c Component object
	 * @param g Graphics object
	 * @param x coordinate
	 * @param y cooridnate
	 */
	public void paintIcon(Component c, Graphics g, int x, int y)
	{
		Graphics2D g2 = (Graphics2D) g;
		shape.draw(g2);
	}

	private int width;
	private int height;
	private MoveableShape shape;
}


