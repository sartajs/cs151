import java.util.*;

public interface IntSequence {
	/**
	 * abstract method not implemented
	 * @param nothing
	 * @return nothing
	 */
	Integer next();
	/**
	 * creates intsequence from iterator
	 * @param iterator
	 * @return intsequence from iterator
	 */
	static IntSequence fromIterator(Iterator<Integer> iterator) {
		return () -> iterator.next();
	}
	/**
	 * returns alternating values from curr. sequence and other
	 * @param other sequence
	 * @return alternating elements from both.
	 */
	default IntSequence alternate (IntSequence other) {
		final int start[]= {0};
		final Integer stor [] = {0};
		ArrayList<Integer> seq = new ArrayList<Integer>();
		IntSequence temp = () -> {
			while(stor[0]==null){
				
				if(start[0]==0 ) {
					start[0]++;
					stor[0]=this.next();
				}
				else if (start[0]==1) {
					start[0]--;
					stor[0]=other.next();
				}

			}
			return stor[0];
		};
		return temp;


	}
}
