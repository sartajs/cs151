function AbstractGame (game) {
  return{
    winningMove : (pos) => {
      for (const n of this.game.move(pos)) {  
        if (this.game.losingPosition(n)) return n // Can force the opponent to lose
      }
      return undefined // Couldn't find a winning move
    },
  
    losingPosition : (pos) => {
      if (this.game.win(pos)) return true // Just lost
      for (const n of this.game.move(pos)) { 
        if (this.game.winningMove(n) === undefined) return false // Not losing here
      }
      return true // Lose no matter what
    } 
  }
}

function NimGame(){
    return{
    move : (p) => {
      const result = []
      for (let i = p - 1; 2 * i >= p; i--)
        result.push(i)
      return result
    },
    win: (p) => {
      return p === 1
    }
  }
}

let myGame = AbstractGame(NimGame())

for (let p = 1; p <= 16; p++) {
  console.log(p + ' -> ' + myGame.winningMove(p))
}