import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
/**
 * 
 * @author Taj
 *
 */
public class MoveableIcon extends ImageIcon implements MoveableShape{

	/**
	 * draws the MoveableIcon
	 * @param g2 the drawer
	 */
	public void draw(Graphics2D g2) {
		g2.drawImage(this.getImage(),this.x,this.y,null);

	}
	/**
	 * getter for x coordinate
	 * @return x the x coordinate
	 */
	public int getX() {
		return x;
	}
	/**
	 * getter for y coordinate
	 * @return y the y coordinate
	 */
	public int getY() {
		return y;
	}
	/**
	 * Yields the bounding rectangle of this shape.
	 * @return the bounding rectangle
	 */
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.getIconWidth(),this.getIconHeight());
	}
	/**
	 * creates icon of the given filename with the given coordinates
	 * @param filename filename of icon to be used as origin
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public MoveableIcon(String filename, int x, int y)
	{
		super(filename);
		this.x = x;
		this.y = y;
	}
	/**
	 * moves the icon randomly
	 */
	public void move() {
		if(Math.random()<0.5) {
			x++;
			y--;
		}
		else
			x--;
		y++;

	}

	private int x;
	private int y;
}
