import java.util.Arrays;

/**
 * @author Taj
 * ArraySet implementation of IntSet
 */
public class ArraySet implements IntSet
{
	/**
	 * constructor for ArraySet
	 * @param nothing
	 * @return nothing
	 */
	public ArraySet() {
		elementCount=0;
		elements = new int[10];
	}
	/**
	    * test if value is present in the ArraySet
	    * @param value to be tested
	    * @return true if found
	    */
	public boolean test(int n)
	{
		if (n>largest || n<smallest)
			return false;

		for(int i=0; i<elementCount;i ++) {
			if (elements[i]==n)
				return true;
		}
		return false;
	}
	/**
	    * removes value in arrayset by swapping it w/ last element in set
	    * @param value to be removed
	    * @return nothing
	    */
	public void clear(int n) 
	{
		int index=0;
		for(int i=0; i<elementCount;i ++) {
			if (elements[i]==n) {
				index=i;
				break;
			}
		}
		elements[index] = elements[elementCount-1];
		elementCount--;
		if(largest==n)
			findLarge();
		else if(smallest==n)
			findSmall();
	}

	/**
	    * finds/sets max in set
	    * @param nothing
	    * @return nothing
	    */
	public void findLarge() {
		largest = Integer.MIN_VALUE;
		for(int i=0; i<elementCount; i ++) {
			if (elements[i]>largest)
				largest=elements[i];
		}
	}
	/**
	    * finds/sets min in set
	    * @param nothing
	    * @return nothing
	    */
	public void findSmall() {
		smallest = Integer.MAX_VALUE;
		for(int i =0; i<elementCount; i++) {
			if (elements[i]<smallest)
				smallest=elements[i];
		}
	}
	/**
	    * appends value to set
	    * @param value to be added
	    * @return nothing
	    */
	public void set(int n)
	{
		//add value
		elements[elementCount]=n;
		elementCount++;

		//update smallest, largest
		if (smallest>n)
			smallest=n;
		if (largest<n)
			largest=n;

		//updates array length if too large.
		if(elementCount==elements.length) {
			elements=Arrays.copyOf(elements, elementCount*2);
		}

	}

	// Don't change any of these (but add javadoc)
	/**
	    * returns min value in set
	    * @param nothing
	    * @return smallest val
	    */
	public int min()
	{
		return smallest;
	}
	/**
	    * returns largest value in set
	    * @param nothing
	    * @return largest val
	    */
	public int max()
	{
		return largest;
	}
	/**
	    * returns size of set
	    * @param nothing
	    * @return size of set
	    */
	public int size()
	{
		return elementCount;
	}

	// These are left package visible so they can be accessed in a unit test
	int smallest = Integer.MAX_VALUE;
	int largest = Integer.MIN_VALUE;
	int[] elements;
	int elementCount;
}
