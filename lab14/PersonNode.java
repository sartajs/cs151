import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.net.URL;

import javax.swing.ImageIcon;

public class PersonNode  extends CircleNode{

	public PersonNode() {
		super(Color.white);
		super.setSize(40);
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
		
		try {
			icon = new ImageIcon(new URL(imageURL));
		}
		
		catch(Exception e) {
			
		}
		
	}

	public void draw(Graphics2D g2) {
		
		//icon.paintIcon(icon, g2,);
	}
	private String imageURL;
	private ImageIcon icon;

}
