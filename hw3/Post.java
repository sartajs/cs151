import java.util.ArrayList;

/**
 * a question entry/post on the piazza forum in a given class
 */
public class Post {

	String text;
	int good=0;
	int views=0;
	Response instructor = new Response();
	Response student = new Response();
	ArrayList <User> editors = new ArrayList<User>();
	ArrayList <User> viewers = new ArrayList<User>();
	ArrayList <FollowupDiscussion> discussions = new ArrayList<FollowupDiscussion>();
	
	/**
	    * update post with new author and text
	    * @param user editting and their input
	    * @return 
	    */
	public void update(User currentUser, String string) {
		text=string;
		editors.add(currentUser);
		
	}
	
	/**
	    * returns instructor response
	    * @param 
	    * @return response of the instructor
	    */
	public Response getInstructorResponse() {
		return instructor;
	}

	/**
	    * returns student response
	    * @param 
	    * @return response of the instrcutor
	    */
	public Response getStudentResponse() {
		return student;
	}

	/**
	    * gets text of the post
	    * @param 
	    * @return returns the text of the post
	    */
	public Object getText() {
		return text;
	}

	/**
	    * increase the good votes for the post
	    * @param 
	    * @return 
	    */
	public void setGood() {
		good++;
		
	}

	/**
	    * return username of the last editor
	    * @param 
	    * @return returns the last editor of the post
	    */
	public String getLastAuthor() {
		return editors.get(editors.size()-1).getUsername();
	}

	/**
	    * increment view count if new user views
	    * @param user viewing post
	    * @return view count
	    */
	public int view(User currentUser) {
		if(!viewers.contains(currentUser))
			views++;
		return views;
	}

	/**
	    * add a followup discussion for further discussion
	    * @param the follow up discussion object to be added
	    * @return the position of the new discussion object
	    */
	public int add(FollowupDiscussion followup) {
		discussions.add(followup);
		return discussions.size()-1;
	}

	/**
	    * return count of the number of discussions
	    * @param 
	    * @return discussion count
	    */
	public int getFollowupCount() {
		return discussions.size();
	}

	/**
	    * returns follow up discussion at given id
	    * @param index desired
	    * @return discussion at given index
	    */
	public FollowupDiscussion getFollowup(int followupid) {
		return discussions.get(followupid);
	}

	/**
	    * checks to see if a post is good
	    * @param 
	    * @return true if post has any good votes
	    */
	public boolean isGood() {
		return (good>0);
	}

}
