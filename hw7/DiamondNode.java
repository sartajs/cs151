import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
/**
 * A class that creates a diamond node
 * @author Taj
 *
 */
public class DiamondNode implements Node{
   /**
   Construct a diamond node
    */
   public DiamondNode() {
      x = 0;
      y = 0;
      size=DEFAULT_SIZE;
      lines = new ArrayList <Line2D.Double>(4);
      for(int i = 0; i < 4 ; i++) {
         lines.add(new Line2D.Double());
      }
      lines.get(0).setLine(x+size/2,y, x+size, y + size/2);
      lines.get(1).setLine(x+size,y+size/2, x+size/2, y + size);
      lines.get(2).setLine(x+size/2,y+size, x, y + size/2);
      lines.get(3).setLine(x,y+size/2, x+size/2, y);
   }
   /**
    * Another constructor that takes in x and y values
    * @param z the x value
    * @param v the y value
    */
   public DiamondNode(double z, double v) {
      this.x = z;
      this.y = v;
      size=DEFAULT_SIZE;
      lines = new ArrayList <Line2D.Double>(4);
      for(int i = 0; i < 4 ; i++) {
         lines.add(new Line2D.Double());
      }
      lines.get(0).setLine(x+size/2,y, x+size, y + size/2);
      lines.get(1).setLine(x+size,y+size/2, x+size/2, y + size);
      lines.get(2).setLine(x+size/2,y+size, x, y + size/2);
      lines.get(3).setLine(x,y+size/2, x+size/2, y);
   }
   /**
    * Clones object
    * @return returns the object cloned
    */
   public Object clone()
   {
      try
      {
         return super.clone();
      }
      catch (CloneNotSupportedException exception)
      {
         return null;
      }
   }

   /**
    * Draws the CircleNode object
    * @param g2 the Graphics2D g2 object
    */
   public void draw(Graphics2D g2) {
      DiamondNode temp = new DiamondNode(x,y);
      temp.lines.get(0).setLine(temp.x+size/2,temp.y, temp.x+size, temp.y + size/2);
      temp.lines.get(1).setLine(temp.x+size,temp.y+size/2, temp.x+size/2, temp.y + size);
      temp.lines.get(2).setLine(temp.x+size/2,temp.y+size, temp.x, temp.y + size/2);
      temp.lines.get(3).setLine(temp.x,temp.y+size/2, temp.x+size/2, temp.y);
      for(int i = 0; i < 4 ; i++) {
         g2.draw(temp.lines.get(i));
      }

   }

   /**
    * translates object to new position
    * @param dx the new x position
    * @param dy the new y position
    */
   public void translate(double dx, double dy) {
      x+=dx;
      y+=dy;
   }

   /**
    * checks to see if the the given point is contained within the given object
    * @param aPoint the point
    * @return true if the point is contained
    */
   public boolean contains(Point2D aPoint) {
      DiamondNode temp = new DiamondNode(x,y);
      int [] xCords = new int[4];
      int [] yCords = new int[4];
      for(int i = 0; i < 4; i++) {
         xCords[i] = Math.round(Math.round(temp.lines.get(i).getX1()));
         yCords[i] = Math.round(Math.round(temp.lines.get(i).getY1()));
      }
      Polygon temp1 = new Polygon(xCords, yCords, 4);
      return temp1.contains(aPoint);


   }

   /**
    * gets the connection point to the other point
    * @param other the other point to be connected
    * @return the point at which the other point should connect to this.
    */
   public Point2D getConnectionPoint(Point2D other) {
      DiamondNode temp = new DiamondNode(x,y);
      double centerX = x + size / 2;
      double centerY = y + size / 2;
      double dx = other.getX() - centerX;
      double dy = other.getY() - centerY;
      double distance = Math.sqrt(dx * dx + dy * dy);
      if (distance == 0) return other;

      if(dx>dy && dx <= -dy)
         return new Point2D.Double(temp.lines.get(0).getX1(), temp.lines.get(0).getY1());
      if(dx>=dy && dx >= -dy)
         return new Point2D.Double(temp.lines.get(1).getX1(), temp.lines.get(1).getY1());
      if(dx<=dy && dx>-dy)
         return new Point2D.Double(temp.lines.get(2).getX1(), temp.lines.get(2).getY1());

      else  return new Point2D.Double(temp.lines.get(3).getX1(), temp.lines.get(3).getY1());


   }
   /**
    * gets the bounds of the DiamondNode object
    * @return Rectangle2D that is a bound to this object
    */
   public Rectangle2D getBounds() {
      return new Rectangle2D.Double(
            x, y, size, size);
   }

   private List<Line2D.Double> lines ;
   private double x;
   private double y;
   private double size;
   private static final int DEFAULT_SIZE = 20;
}
