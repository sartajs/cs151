// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/Taj/cs151/lab13/conf/routes
// @DATE:Tue Mar 19 16:00:03 PDT 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
