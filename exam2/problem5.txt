problem5.txt

Not quite. LineStyle essentially is "common" to all objects of Edges. Whenever a new line edge is created, so is a new stroke. So, intrinsically it is a clone, much like its line that encapsulates it. BUT, when a 'new' stroke is required, it is not cloned. You cannot clone it within the object itself so it is not a true example of the prototype pattern.