'use strict'
function findNode(g, x, y){
	for(let i = g.nodes.length - 1; i>=0; i--){
		const n = g.nodes[i]
		if(n.x === x && n.y === y ) return n
	}
	return undefined

}

module.exports = { findNode }


