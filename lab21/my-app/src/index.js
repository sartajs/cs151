import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square {
  constructor(color, text) {
    this.color = color
    this.text = text
  }
  getProperties() { return [
    {name: 'color', value: this.color },
    {name: 'text', value: this.text }] }    
}

class SquareComponent extends React.Component {
  render() {
    return (
        <div
      style={{background: this.state.data.color }}
      className="square">
        { this.state.data }
      </div>
    );
  }
}
// https://medium.com/capital-one-tech/how-to-work-with-forms-inputs-and-events-in-react-c337171b923b

class TextEditor extends React.Component {
  constructor(props) {
    super(props)
    this.state = { text: this.props.dataProps.value }  
  }
  
  handleChange(event) {
    this.setState({text: event.target.value})
    this.props.changeListener(this.props.dataProps.name, event.target.value)
  }
  
  render() {
    return <input type='text' value={this.state.text} onChange={event => this.handleChange(event) } />
  }
}
class Row extends React.Component {
  render() {
    return (
        <tr><td>{this.props.dataProps.name}</td><td>{this.props.dataProps.value}</td></tr>
      )
  }
}

class Sheet extends React.Component {
  render() {
    return (<table>{this.props.dataProps.map(p => <Row dataProps={p} changeListener={this.props.changeListener} />)}</table>)
  }
}
class Game extends React.Component {
    constructor(props) {
    super(props)
    this.state = { data: new Square('blue', 'Fred') }
  }

  // https://medium.com/@ruthmpardee/passing-data-between-react-components-103ad82ebd17
  squareChangeListener(name, value) {
    // ...
    this.setState({data: this.state.data})
  }

  render() {
    return (
      <div className="game">
      <SquareComponent data={new Square('blue', 'Fred')}/>
      <Sheet dataProps={this.state.data.getProperties()} changeListener={ (n, v) => this.squareChangeListener(n, v) } />
        <div className="game-board">
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
