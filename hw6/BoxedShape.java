import java.awt.Graphics2D;
import java.awt.Rectangle;
/**
 * creating a box around the given shape
 * @author Taj
 *
 */
public class BoxedShape implements MoveableShape{
	/**
	 * constructor for BoxedShape that takes in the shape and desired gap
	 * @param shape shape around which to construct box
	 * @param gap the gap between the given shape and rectangle
	 */
	public BoxedShape(MoveableShape shape, int gap){
		rectangle = shape.getBounds();;
		rectangle.grow(gap,gap);
		this.shape = shape;
		this.gap = gap;


	}
	/**
	 *  draws the shape and rectangle
	 * @param g2 the graphics drawer
	 */
	public void draw(Graphics2D g2) {
		shape.draw(g2);
		g2.draw(this.rectangle);
	}
	/**
	 * moves the shape and rectangle with the given shaoe's move function
	 */
	public void move() {
		shape.move();
		rectangle = shape.getBounds();
		rectangle.grow(gap,gap);
	}
	/**
	 * returns bounding rectangle
	 * @return rectangle gives the rectangle that bounds this BoxedShape
	 */
	public Rectangle getBounds() {
		return rectangle;
	}


	private MoveableShape shape;
	Rectangle rectangle; 
	int gap;
}
