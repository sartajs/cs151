import java.util.List;
/**
 * A freeflow random move strategy
 * @author Taj
 *
 */
public class SimpleMoveStrategy implements MoveStrategy{
	/**
	 * moves shapes using their move method
	 * @param shapes list of shapes to be moved
	 */
	public void process(List<MoveableShape> shapes) {

		for(MoveableShape x : shapes)
			x.move();

	}
}
