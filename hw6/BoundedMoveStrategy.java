import java.awt.Rectangle;
import java.util.List;
/**
 * Move strategy that stops once shape reaches edge of given rectangle
 * @author Taj
 *
 */
public class BoundedMoveStrategy implements MoveStrategy{

	/**
	 * constructor that takes in the confining rectangle.
	 * @param rectangle given rectangle that confines movement
	 */
	public BoundedMoveStrategy(Rectangle rectangle) {
		this.rectangle = rectangle;
	}
	/**m
	 * moves while shape is contained within given rectangle
	 * @param shapes collection of shapes
	 */
	public void process(List<MoveableShape> shapes) {
			for(MoveableShape x : shapes)
				if(rectangle.contains(x.getBounds()))
					x.move();
	}
	Rectangle rectangle;

}
