All ok with git (grader added to repo, directory, filename) (max. 4): 4

piazza.png
A post has two responses (max. 2): 2
A post has 0 or more followup discussions (max. 1): 1
A followup discussion has 0 or more submissions/followup responses (max. 1): 1
A Piazza simulation has 0 or more posts (max. 1): 0
A followup discussion can be resolved (max. 1): 0
A post can be good, a response can be good (max. 2): 2
(This can be achieved by separate attributes or inheritance.)
A post, response, followup discussion, submission/followup response,  have an author/updater/User (max. 4): 4
(This can be achieved by an attribute or aggregation with User,
or if they inherit from a class with with such an attribute or
aggregation.)

change.png
Three timelines for Telephone, Connection, Mailbox (max. 3): 3
Colons before class names (: Telephone) (max. 1): 1
dial/checkPasscode/speak as in solution (max. 2): 1
dial/speak/record as in solution (max. 2): 0
dial/setGreeting as in solution (max. 2): 2
(If the self calls aren't there, we let it go this time.)

