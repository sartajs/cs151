import java.util.ArrayList;

/**
 * An official answer to a post, also to followup discussions
 */
public class Response {

	String text;
	int good=0;
	User editors;
	
	/**
	    * default response constructor
	    * @param 
	    * @return 
	    */
	public Response() {
		
	}
	/**
	    * create response from a given user's input
	    * @param user typing the text and the text itself
	    * @return 
	    */
	public Response(User currentUser, String string) {
		text = string;
		editors=currentUser;
		
	}

	/**
	    * increment the good vote for response count
	    * @param 
	    * @return 
	    */
	public void setGood() {
		good++;
		
	}
	/**
	    * get the author of the response
	    * @param 
	    * @return author of the response
	    */
	public String getAuthor() {
		return editors.getUsername();
	}

	/**
	    * update the response with new text and a new updated author
	    * @param new author and new text for response
	    * @return 
	    */
	public void update(User currentUser, String string) {
		text = string;
		editors=currentUser;
		
	}
	
	/**
	    * get text in the response
	    * @param 
	    * @return text in the response
	    */
	public Object getText() {
		return text;
	}

	/**
	    * check to see if the post has good votes
	    * @param 
	    * @return true if the post any good votes
	    */
	public boolean isGood() {
		return (good>0);
	}

	/**
	    * get the username of the last person to edit the response
	    * @param 
	    * @return username of last editor
	    */
	public String getLastAuthor() {
		return editors.getUsername();
	}

}
