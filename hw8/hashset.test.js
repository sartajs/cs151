'use strict'
const { createHashSet } = require('./hashset')
function hashCode (x) { return x}
function equals(x, y) { return x===y}
let set = createHashSet(5, hashCode, equals)

set.add(0)
set.add(10)
set.add(2)
set.add(12)
set.add(22)

let iterator = set.iterator()

test('testing', () => {
	expect (iterator.next()).toBe(10),
	expect (iterator.next()).toBe(0),
	expect (iterator.next()).toBe(22),
	expect (iterator.next()).toBe(12),
	expect (iterator.next()).toBe(2)

	})
iterator = set.iterator()
iterator.next()
iterator.remove
iterator = set.iterator()
test('removal testing', () => {
	expect (iterator.next()).toBe(0)
	})