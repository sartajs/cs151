import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.Test;

class IteratorTest {

	@Test
	void arraytest() {
		IntSet set = new ArraySet();
		Iterator<Integer> iterator = set.iterator();
		
		set.set(10);
		set.set(13);
		set.set(9);
		set.set(6);
		set.set(15);
		
		assertEquals(true,set.test(15));
		assertEquals(true,set.test(9));
		assertEquals(true,set.test(6));
		assertEquals(true,set.test(13));
		
		assertEquals(15, set.max());
		assertEquals(6, set.min());
		
		assertEquals(true,iterator.hasNext());
		
		assertEquals(10, iterator.next().intValue());
		iterator.remove();
		assertEquals(13, iterator.next().intValue());	
		iterator.remove();
		assertEquals(9, iterator.next().intValue());
		iterator.remove();

		

	}
	
	@Test
	void bittest() {
		BitSet set = new BitSet();
		Iterator<Integer> iterator = set.iterator();
		set.set(100);
		set.set(102);
		set.set(104);
		set.set(101);
		
		assertEquals(true,set.test(100));
		assertEquals(true,set.test(102));
		assertEquals(false,set.test(99));
		assertEquals(true,set.test(101));
		
		assertEquals(100, iterator.next().intValue());
		assertEquals(100, set.min());
		iterator.remove();
		assertEquals(101, set.min());
		assertEquals(101, iterator.next().intValue());
		iterator.remove();
		assertEquals(102, iterator.next().intValue());
		assertEquals(102, set.min());
		iterator.remove();
		assertEquals(104, iterator.next().intValue());
		assertEquals(104, set.min());


	}
	
	
	@Test 
	void shouldThrowNoSuchElement() {
		IntSet set = new ArraySet();
		Iterator<Integer> iterator = set.iterator();
		set.set(10);
		iterator.next();
		assertThrows(NoSuchElementException.class,() -> iterator.next());

	}
}
