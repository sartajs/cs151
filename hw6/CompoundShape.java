import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
/**
 * A shape containing the other shapes
 * @author Taj
 *
 */
public class CompoundShape implements MoveableShape{

	/**
	 * constructor that adds the shapes to the 'compound'
	 * @param shapes
	 */
	public CompoundShape (MoveableShape...shapes) {
		for(MoveableShape x : shapes) {
			this.shapes.add(x);
		}
	}
	/**
	 * draws the CompoundShape
	 * @param g2 the drawer
	 */
	public void draw(Graphics2D g2) {
		for(MoveableShape x : shapes) {
			x.draw(g2);
		}
		
	}

	/**
	 * moves shapes within the compound shape as they should
	 */
	public void move() {
		for(MoveableShape x : shapes) {
			x.move();
		}
	}

	/**
	 * gets the bounds of the CompoundShape
	 * @return rectangle the smallest rectangle containing all the shapes
	 */
	public Rectangle getBounds() {
		Rectangle temp;
		double largestW=0;
		double largestH=0;
		double smallestX=0;
		double smallestY=0;
		for(MoveableShape x : shapes) {
			temp = x.getBounds();
			
			if(smallestY>temp.getY()) {
				smallestY=temp.getMinY();
			}
			if(smallestX>temp.getX()) {
				smallestX=temp.getMinX();
			}
			if(largestW<temp.getMaxX()) {
				largestW=temp.getMaxX();
			}
			if(largestH<temp.getMaxY()) {
				largestH=temp.getMaxY();
			}
			
		}
		this.largestW = Math.round(Math.round(largestW));
		this.largestH = Math.round(Math.round(largestH));
		this.smallestX = Math.round(Math.round(smallestX));
		this.smallestY = Math.round(Math.round(smallestY));
		return new Rectangle(this.smallestX, this.smallestY, this.largestW, this.largestH);
	}

	ArrayList<MoveableShape> shapes = new ArrayList<MoveableShape>();
	private int largestW=0;
	private int largestH=0;
	private int smallestX=0;
	private int smallestY=0;
}
