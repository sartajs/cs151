import java.awt.*;
import java.awt.geom.*;

/**
   An edge that is shaped like a straight line.
 */
public class LineEdge extends AbstractEdge
{
   /**
    * method to draw an edge to between the connection points of the objects
    * @param g2 the Graphics2D object
    */
   public void draw(Graphics2D g2)
   {
      g2.draw(getConnectionPoints());
   }
   /**
    * checks to see if the edge contains the given point
    * @param aPoint the point to be checked
    * @return true if it does. false if not.
    */
   public boolean contains(Point2D aPoint)
   {
      final double MAX_DIST = 2;
      return getConnectionPoints().ptSegDist(aPoint) 
            < MAX_DIST;
   }
}
