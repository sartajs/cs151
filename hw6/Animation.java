import java.awt.*;
import java.util.ArrayList;
import java.util.List;


import javax.swing.*;
/**
   This program implements an animation that moves
   a car shape.
 */
public class Animation
{
	/**
	 * 
	 * @param shapes collection of the shapes
	 * @param strategy type of move strategy
	 * @param width width of the JLabel
	 * @param height height of the Jlabel
	 */
	public static void show(List<MoveableShape> shapes, MoveStrategy strategy, int width, int height)
	{
		JFrame frame = new JFrame();
		final ArrayList<ShapeIcon> icons = new ArrayList<ShapeIcon>();
		for(MoveableShape x : shapes) {
			icons.add( new ShapeIcon(x, width, height));
		}


		final ArrayList<JLabel> labels = new ArrayList<JLabel>();
		for(ShapeIcon x : icons ) {
			labels.add( new JLabel(x));
		}

		frame.setLayout(new FlowLayout());

		for(JLabel x : labels) {
			frame.add(x);
		}
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

		final int DELAY = 100;
		// Milliseconds between timer ticks
		Timer t = new Timer(DELAY, event ->
		{
			strategy.process(shapes);

			for(JLabel x : labels)
				x.repaint();
		});
		t.start();
	}
}
