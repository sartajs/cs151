import java.util.ArrayList;
/**
 * A follow up discusion that can be created within a post
 */
public class FollowupDiscussion {

	private boolean resolved=false;
	private String text;
	private User author;
	private ArrayList<Response> responses= new ArrayList<Response>();
	
	/**
	    * update/create the discussion beginning
	    * @param  creating user and the input from the user
	    * @return 
	    */
	public void update(User currentUser, String string) {
		author = currentUser;
		text = string;
		
	}
	
	/**
	    * get the text in the follow up discussion header
	    * @param 
	    * @return text of the header
	    */
	public String getText() {
		return text;
	}
	
	/**
	    * get the last author of the text in followup discussion
	    * @param 
	    * @return authors username
	    */
	public String getAuthor() {
		return author.getUsername();
	}

	/**
	    * checks to see if the followup discussion has been resolved
	    * @param 
	    * @return true if marked as resolved 
	    */
	public boolean isResolved() {
		return resolved;
	}

	/**
	    * add a response to the followup discussion
	    * @param response object
	    * @return index location of the response in the followup discussion object
	    */
	public int addResponse(Response r) {
		responses.add(r);
		return responses.size()-1;
	}

	/**
	    * gets the number of responses to the discussion
	    * @param 
	    * @return count of responses to the discussion
	    */
	public int getResponseCount() {
		return responses.size();
	}
	
	/**
	    * get response at the given index and return it
	    * @param desired response index
	    * @return response at given index
	    */
	public Response getResponse(int followupresponseid) {
		return responses.get(followupresponseid);
	}

	/**
	    * mark the discussion as resolved
	    * @param 
	    * @return 
	    */
	public void resolve() {
		resolved=true;
	}

}
