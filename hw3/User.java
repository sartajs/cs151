/**
 * A piazza user that is username+password protected
 */
public class User {

	enum Type{
		INSTRUCTOR, STUDENT;
	}
	
	private String username, password;
	Type type;
	
	/**
	    * create User object
	    * @param users attributes
	    * @return uer with attributes
	    */
	public User(String username, String password,Type type) {
		this.type = type;
		this.username=username;
		this.password=password;
		
	}
	/**
	    * return username
	    * @param 
	    * @return username of user
	    */
	public String getUsername() {
		return username;
	}
	/**
	    * check username and password
	    * @param username and password entry
	    * @return authentication result
	    */
	public boolean authenticate(String username, String password) {
		if(this.username.equals(username)) {
			return(this.password.equals(password));
		}
			
		return false;
	}
	/**
	    * check to see if User is instructor
	    * @param 
	    * @return boolean of if user is instructor or not
	    */
	public boolean isInstructor() {
		return this.type==Type.INSTRUCTOR;
	}

}
