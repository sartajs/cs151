class AbstractGame {
  winningMove(pos) {
    for (const n of this.move(pos)) {  
      if (this.losingPosition(n)) return n // Can force the opponent to lose
    }
    return undefined // Couldn't find a winning move
  }
  
  losingPosition(pos) {
    if (this.win(pos)) return true // Just lost
    for (const n of this.move(pos)) { 
      if (this.winningMove(n) === undefined) return false // Not losing here
    }
    return true // Lose no matter what
  }  
}
class NimGame extends AbstractGame {
  move(p) {
    const result = []
    for (let i = p - 1; 2 * i >= p; i--)
      result.push(i)
    return result
  }
  win(p) {
    return p === 1
  }
}

const myGame = new NimGame()

for (let p = 1; p <= 16; p++) {
  console.log(p + ' -> ' + myGame.winningMove(p))
}