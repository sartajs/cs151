import java.awt.*;
import java.awt.geom.*;

/**
   A class that supplies convenience implementations for 
   a number of methods in the Edge interface type.
 */
public abstract class AbstractEdge implements Edge
{  
   /**
    * Clones object
    * @return returns the object cloned
    */
   public Object clone()
   {
      try
      {
         return super.clone();
      }
      catch (CloneNotSupportedException exception)
      {
         return null;
      }
   }
   /**
    * Connects two nodes
    * @param s start node
    * @param e end node
    */
   public void connect(Node s, Node e)
   {  
      start = s;
      end = e;
   }
   /**
    * gets start node
    * @return the start node
    */
   public Node getStart()
   {
      return start;
   }
   /**
    * gets end node
    * @return the end node
    */
   public Node getEnd()
   {
      return end;
   }
   /**
    * gets the bounding rectangle
    * @param g2 the Graphics2D object
    * @return the bounding rectangle of the edge
    */
   public Rectangle2D getBounds(Graphics2D g2)
   {
      Line2D conn = getConnectionPoints();      
      Rectangle2D r = new Rectangle2D.Double();
      r.setFrameFromDiagonal(conn.getX1(), conn.getY1(),
            conn.getX2(), conn.getY2());
      return r;
   }
   /**
    * gets the connection line between the points
    * @return the line connecting the points
    */
   public Line2D getConnectionPoints()
   {
      Rectangle2D startBounds = start.getBounds();
      Rectangle2D endBounds = end.getBounds();
      Point2D startCenter = new Point2D.Double(
            startBounds.getCenterX(), startBounds.getCenterY());
      Point2D endCenter = new Point2D.Double(
            endBounds.getCenterX(), endBounds.getCenterY());
      return new Line2D.Double(
            start.getConnectionPoint(endCenter),
            end.getConnectionPoint(startCenter));
   }

   private Node start;
   private Node end;
}
