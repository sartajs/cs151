import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SequenceTetst{


	@Test
	void test() {
		IntSet set = new ArraySet();
		Iterator<Integer> iterator = set.iterator();

		set.set(1);
		set.set(2);
		set.set(3);
		set.set(4);
		set.set(5);

		IntSet set2 = new ArraySet();
		Iterator<Integer> iterator2 = set2.iterator();

		set2.set(5);
		set2.set(10);
		set2.set(15);
		set2.set(20);
		set2.set(25);


		IntSequence sequencer = IntSequence.fromIterator(iterator);
		assertEquals((Integer) 1,sequencer.alternate(IntSequence.fromIterator(iterator2)));
		}
	}


