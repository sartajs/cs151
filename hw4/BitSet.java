import java.util.Arrays;

/**
 * @author Taj
 * BitSet implementation of IntSet
 */
public class BitSet implements IntSet
{
	/**
	    * BitSet constructor
	    * @param nothing
	    * @return nothing
	    */
	public BitSet() {
		elementCount = 0;
		start = 0;
		elements = new int[10];

	}
	/**
	    * tests if element is in the BitSet
	    * @param element to look for
	    * @return true if found, false if not
	    */
	public boolean test(int n)
	{
		for(int i = 0; i<elements.length;i++) {
			if(test(n, n/32 -1)) {
				return true;
			}
		}
		return false;
	}
	/**
	    * adds the element to the set
	    * @param element to be inserted
	    * @return nothing
	    */
	public void set(int n)
	{
		if(elementCount==0) {
			start = n;
		}
		while((elements.length*32+start-1)<n) {
			elements = Arrays.copyOf(elements, elements.length*2);
		}
		while(n<start) {
			start += n*(-32);
			elements = Arrays.copyOf(elements, elements.length*2);
		}

		elements[elementCount]=set(elementCount,n/32 -1);
		elementCount++;

	}   
	/**
	    * removes element from the set by switching it with the last
	    * @param element to be found and removed
	    * @return nothing
	    */
	public void clear(int n)
	{
		for(int i=0;i<elementCount;i++) {
			if(test(elements[i],n%32)) {
				clear(elements[i],n%32);
				elements[i]=set(elements[elementCount],n%32);
				elementCount--;
			}

		}
	}
	/**
	    * finds the minimum element in bitset
	    * @param nothing
	    * @return the start value, aka minimum
	    */
	public int min()
	{
		return start;
	}
	/**
	    * returns the largest element stored in the bitset
	    * @param nothing
	    * @return max
	    */
	public int max()
	{
		int max = start;
		for(int i : elements) {
			if(i>max)
				max = elements[i];
		}

		return max;
	}

	// Don't change any of these (but add javadoc)
	/**
	    * gets size of BitSet
	    * @param nothing
	    * @return size
	    */
	public int size()
	{
		return elementCount;
	}
	/**
	    * checks if the value at a certain location is the same
	    * @param the location and the element to be checked for
	    * @return true if present
	    */
	private static boolean test(int n, int i)
	{
		assert 0 <= i && i < 32;
		return (n & (1 << i)) != 0;
	}
	/**
	    * sets value at certain location
	    * @param the location and the element to be inserted
	    * @return set value
	    */
	private static int set(int n, int i)
	{
		assert 0 <= i && i < 32;
		return n | (1 << i);
	}

	/**
	    * removes value at certain location
	    * @param location and value to removed
	    * @return removed value
	    */
	private static int clear(int n, int i)
	{
		assert 0 <= i && i < 32;
		return n & ~(1 << i);
	}

	// These are left package visible so they can be accessed in a unit test

	int[] elements;
	int start;
	int elementCount;
}
