import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BitSet implements IntSet
{
	/**
	 * checks for n in set
	 * @param n to be tested
	 * @return true if found
	 */
	public boolean test(int n)
	{
		if (elements == null || n < start || n >= start + 32 * elements.length)
			return false;
		int p = (n - start) / 32;
		int i = (n - start) % 32;
		return test(elements[p], i);
	}
	/**
	 * adds n to set
	 * @param n
	 * @return nothing
	 */
	public void set(int n)
	{
		if (elements == null) 
		{
			elements = new int[10];
			start = n;
		}
		else if (n < start)
		{
			int intsNeeded = (start + 32 * elements.length - n) / 32 + 1;
			int[] newElements = new int[Math.max(intsNeeded, 2 * elements.length)];
			System.arraycopy(elements, 0, 
					newElements, newElements.length - elements.length, 
					elements.length);
			start -= 32 * (newElements.length - elements.length);
			elements = newElements;
		}
		else if (n >= start + 32 * elements.length)
		{
			int intsNeeded = (n - start) / 32 + 1; 
			elements = Arrays.copyOf(elements, 
					Math.max(intsNeeded, 2 * elements.length));         
		}
		int p = (n - start) / 32;
		int i = (n - start) % 32;
		if (!test(elements[p], i)) 
		{
			elementCount++;
			elements[p] = set(elements[p], i);      
		}
	}
	/**
	 * removes n from set
	 * @param n
	 * @return nothing
	 */
	public void clear(int n)
	{
		if (elements != null && n >= start || n < start + 32 * elements.length)
		{
			int p = (n - start) / 32;
			int i = (n - start) % 32;
			if (test(elements[p], i)) 
			{
				elementCount--;
				elements[p] = clear(elements[p], i);
			}
		}
	}
	/**
	 * finds min
	 * @param nothing
	 * @return min
	 */
	public int min()
	{
		if (elements != null)
			for (int p = 0; p < elements.length; p++)
				for (int i = 0; i < 32; i++)
					if (test(elements[p], i)) return 32 * p + i + start;      
		return Integer.MAX_VALUE;
	}
	/**
	 * finds max
	 * @param nothing
	 * @return max
	 */
	public int max()
	{
		if (elements!= null)
			for (int p = elements.length - 1; p >= 0; p--)
				for (int i = 31; i >= 0; i--)
					if (test(elements[p], i)) return 32 * p + i + start;
		return Integer.MIN_VALUE;
	}
	/**
	 * returns size
	 * @param nothing
	 * @return size
	 */
	public int size()
	{
		return elementCount;
	}
	/**
	 * tests for element at given location
	 * @param element and bits to be tested
	 * @return true if found
	 */
	private static boolean test(int n, int i)
	{
		assert 0 <= i && i < 32;
		return (n & (1 << i)) != 0;
	}
	/**
	 * turns on bits for given element
	 * @param element and bits to be turned on
	 * @return returns element turned on
	 */
	private static int set(int n, int i)
	{
		assert 0 <= i && i < 32;
		return n | (1 << i);
	}
	/**
	 * removes bits at given location
	 * @param location and bits to be removed
	 * @return cleared element
	 */
	private static int clear(int n, int i)
	{
		assert 0 <= i && i < 32;
		return n & ~(1 << i);
	}
	/**
	 * get the number of modifications
	 * @param nothing
	 * @return modCount
	 */
	protected int modCount() {
		return modCount;
	}
	/**
	 * increases modCount
	 * @param nothing
	 * @return nothing
	 */
	protected void incrementModCount() {
		modCount++;
	}
	
	public class BitSetIterator implements Iterator<Integer>, IntSequence{

		private int modCount;
		/**
		 * default constructor
		 * @param nothing
		 * @return nothing
		 */
		BitSetIterator(){
			this.modCount=modCount();
			nextIndex = 0;
			afterNext=false;
		}
		/**
		 * checks if next element is present
		 * @param nothing
		 * @return true if present
		 */
		public boolean hasNext() {
			if (nextIndex<elementCount && this.modCount==modCount())
				return true;
			else
				return false;
		}

		/**
		 * goes to next element and returns
		 * @param nothing
		 * @return element at next
		 */
		public Integer next() {

			if(hasNext() && this.modCount==modCount()) {
				for (int p = nextIndex; p < elements.length; p++)
					for (int i = nextIndexp; i < 32; i++) {
						if (test(elements[p], i)) {
							afterNext=true;
							if(i==31) {
								nextIndex++;
								nextIndexp=0;
							}
							nextIndexp=i+1;
							return 32 * p + i + start;
						}
						nextIndex++;
						nextIndexp=0;
					}
			}
			else
				throw new NoSuchElementException();
			return null;
		}
		/**
		 * removes element
		 * @param nothing
		 * @return nothing
		 */
		public void remove() {
			if(!afterNext) {
				throw new IllegalStateException();
			}

			if(!(modCount==modCount())) {
				throw new ConcurrentModificationException();
			}

			clear(32 * (nextIndex) + nextIndexp-1 + start);
			incrementModCount();
			modCount++;
			afterNext=false;
		}

	}
	/**
	 * creates iterator
	 * @param nothing
	 * @return iterator
	 */
	public Iterator<Integer> iterator(){
		return new BitSetIterator();
	}
	private int modCount;
	private int nextIndex;
	private int nextIndexp;
	private boolean afterNext;
	int[] elements;
	int start;
	int elementCount;
}
