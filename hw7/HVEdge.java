import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
/**
 * Class that will create a horizontal edge followed by a vertical one
 * @author Taj
 *
 */
public class HVEdge extends LineEdge
{
   /**
    * overrides draw method so it draws a horizontal line towards the point then a vertical between the connection points
    * @param g2 the Graphics2D object
    */
   public void draw(Graphics2D g2)
   {
      Line2D tempLine = getConnectionPoints();
      Point2D tempH = new Point2D.Double(tempLine.getX2() ,tempLine.getY1());
      Line2D horizontal = new Line2D.Double(tempLine.getP1(), tempH );
      Line2D vertical = new Line2D.Double(horizontal.getP2(), tempLine.getP2());
      g2.draw(horizontal);
      g2.draw(vertical);
   }

}
