import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import org.junit.jupiter.api.Test;

class MultipleIteratorTest {

	@Test
	void test() {
		List<String> list1 = new ArrayList();
		list1.add("a");
		list1.add("b");
		List<String> list2 = new ArrayList();
		list1.add("1");
		list1.add("2");
		List<String> list3 = new ArrayList();
		list1.add("c");
		list1.add("d");
		
		List < Iterator> tempList = new ArrayList();
		
		tempList.add(list1.iterator());
		tempList.add(list2.iterator());
		tempList.add(list3.iterator());
		MultipleIterator multi = new MultipleIterator(tempList);
		
		assertEquals("a", multi.next());
		assertEquals("b", multi.next());
		assertEquals("1", multi.next());
		assertEquals("2", multi.next());
		assertEquals("c", multi.next());
		assertEquals("d", multi.next());
	}

}
