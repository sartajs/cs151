import java.awt.*;
import java.awt.geom.*;

/**
   An inivisible node that is used in the toolbar to draw an
   edge.
 */
public class PointNode implements Node
{
   /**
      Constructs a point node with coordinates (0, 0)
    */
   public PointNode()
   {
      point = new Point2D.Double();
   }
   /**
    * does nothing, only satisfies interface
    * @param g2 the Graphics2D object
    */
   public void draw(Graphics2D g2)
   {
   }
   /**
    * translates invisible node to the points
    * @param dx the x coordinate
    * @param dy the y cooridinate
    */
   public void translate(double dx, double dy)
   {
      point.setLocation(point.getX() + dx,
            point.getY() + dy);
   }
   /**
    * does nothing, only satisfies interface
    * @param p a 2D point
    * @return false
    */
   public boolean contains(Point2D p)
   {
      return false;
   }
   /**
    * returns a bounding rectangle
    * @return the Rectangle2D object
    */
   public Rectangle2D getBounds()
   {
      return new Rectangle2D.Double(point.getX(), 
            point.getY(), 0, 0);
   }
   /**
    * returns connection point
    * @param other the other point
    * @return point between the two
    */
   public Point2D getConnectionPoint(Point2D other)
   {
      return point;
   }
   /**
    * Clones object
    * @return returns the object cloned
    */
   public Object clone()
   {
      try
      {
         return super.clone();
      }
      catch (CloneNotSupportedException exception)
      {
         return null;
      }
   }

   private Point2D point;
}
