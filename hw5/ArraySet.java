import java.util.*;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArraySet implements IntSet
{
	/**
	 * tests for n in set
	 * @param n
	 * @return true if in set
	 */
	public boolean test(int n)
	{
		if (n < smallest || n > largest) return false;
		for (int i = 0; i < elementCount; i++)
			if (elements[i] == n) return true;
		return false;
	}
	/**
	 * removes n from set
	 * @param n
	 * @return nothing
	 */
	public void clear(int n) 
	{
		for (int i = 0; i < elementCount; i++)
			if (elements[i] == n) 
			{
				elements[i] = elements[elementCount - 1];
				elementCount--;
				if (n == smallest)
				{
					smallest = Integer.MAX_VALUE;
					for (int k = 0; k < elementCount; k++) 
						smallest = Math.min(elements[k], smallest);
				}
				if (n == largest)
				{
					largest = Integer.MIN_VALUE;
					for (int k = 0; k < elementCount; k++) 
						largest = Math.max(elements[k], largest);
				}
			}
	}
	/**
	 * adds n to set
	 * @param n
	 * @return nothing
	 */
	public void set(int n)
	{
		if (!test(n))
		{
			if (elements == null)
			{
				elements = new int[10];
			}
			else if (elements.length == elementCount)
			{
				elements = Arrays.copyOf(elements, 2 * elements.length);
			}
			elements[elementCount] = n;
			smallest = Math.min(smallest, n);
			largest = Math.max(largest, n);
			elementCount++;
		}
	}
	/**
	 * retunrs min
	 * @param nothing
	 * @return min
	 */
	public int min()
	{
		return smallest;
	}
	/**
	 * retunrs max
	 * @param nothing
	 * @return max
	 */
	public int max()
	{
		return largest;
	}
	/**
	 * returns size
	 * @param nothing
	 * @return size
	 */
	public int size()
	{
		return elementCount;
	}
	/**
	 * returnds modification count
	 * @param nothing
	 * @return modCount
	 */
	protected int modCount() {
		return modCount;
	}
	/**
	 * increase modiciation count
	 * @param nothing
	 * @return nothing
	 */
	protected void incrementModCount() {
		modCount++;
	}

	/**
	 * creates iterator
	 * @param nothing
	 * @return iterator
	 */
	public Iterator<Integer> iterator(){
		return new ArraySetIterator();
	}
	// ArraySetIterator----------------------------------------------------------------

	public class ArraySetIterator implements Iterator<Integer>,IntSequence{
		
		private int modCount = modCount();
		/**
		 * default constructor
		 * @param nothing
		 * @return notihng
		 */
		ArraySetIterator(){
			modCount = modCount();
			nextIndex = 0;
			afterNext = false;
		}
		/**
		 * checks if set has next element
		 * @param nothing
		 * @return true if it has next element
		 */
		public boolean hasNext() {
			if (nextIndex<elementCount && this.modCount==modCount())
				return true;
			else
				return false;
		}

		/**
		 * goes to next element
		 * @param nothing
		 * @return next element
		 */
		public Integer next() {
			if(hasNext() && modCount==modCount()) {
				afterNext = true;
				nextIndex++;
				return elements[nextIndex-1];
			}
			else
				throw new NoSuchElementException();
		}
		/**
		 * removes curr. elemenet
		 * @param nothing
		 * @return nothing
		 */
		public void remove() {
			if(!afterNext) {
				throw new IllegalStateException();
			}

			if(!(modCount==modCount())) {
				throw new ConcurrentModificationException();
			}
			else{
				clear(elements[nextIndex-1]);
				modCount++;
				incrementModCount();
				afterNext=false;
			}
		}
	}
	// EndArraySetIterator----------------------------------------------------------------
	
	// Fields-----------------------------------------------------------------------------
	private int modCount = 0;
	private int nextIndex = 0;
	private boolean afterNext;


	int smallest = Integer.MAX_VALUE;
	int largest = Integer.MIN_VALUE;
	int[] elements;
	int elementCount;
	// EndFields--------------------------------------------------------------------------




}
