import java.awt.*;
import java.util.*;

/**
   A simple graph with round nodes and straight edges.
 */
public class SimpleGraph extends Graph
{
   /**
    * gets prototypes
    * @return the prototypes
    */
   public Node[] getNodePrototypes()
   {
      Node[] nodeTypes =
         {
               new CircleNode(Color.BLACK),
               new CircleNode(Color.WHITE),
               new DiamondNode()
         };
      return nodeTypes;
   }
   /**
    * gets edge prototypes
    * @return the edge prototypes
    */
   public Edge[] getEdgePrototypes()
   {
      Edge[] edgeTypes = 
         {
               new LineEdge(),
               new VHEdge(),
               new HVEdge()
         };
      return edgeTypes;
   }
}





