
import java.util.*;

public class Encoding
{
    public static Set<String> morseCodes(int m, int n)
   {
      Set<String> result = new TreeSet<>();
      findCodes(m,n,"",result);
      

      return result;
   }
   
   private static void findCodes(int m, int n, String temp, Set<String> result) {
	   if(m+n==0) {
		   result.add(temp);
	   }
	   
	   else if(m==0) {
		   findCodes( m, n-1,temp+"-",result);
	   }
	   else if(n==0) {
		   findCodes( m-1, n,temp+".",result);
	   }
	   else {
		   findCodes( m, n-1,temp+"-",result);
		   findCodes( m-1, n,temp+".",result);
	   }
	   
   }
}