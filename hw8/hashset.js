'use strict'

function createHashSet(bucketsLength, hashCode, equals){
	//try{
	//let link = {data : undefined, next : undefined }
	let buckets = []
	let size = 0

	for(let i = 0; i < bucketsLength; i++){
		buckets.push({
			data: undefined,
			next: undefined
		})

	}

	return{
		contains : (x) => {
			let n = hashCode(x)
			if( n<0 ){
				n = -n
			}
			n = n % buckets.length
			let current = buckets[n].link
			while(current!==undefined){
				if(equals(x,current.data)){
					return true
				}
				current = current.next
			}
			return false
		},

		add : (x) => {
			let n = hashCode(x)
			if( n<0 ){
				n = -n
			}
			n = n % buckets.length
			//console.log(n)
			let current = buckets[n]
			while(current!== undefined){
				if(equals(x, current.data)){
					return false
				}
				current=current.next
			}
			let temp = {data : x, next : buckets[n]}
			buckets[n] = temp
			size++
			//console.log(buckets[n].data)
			//console.log(buckets[n].next.data)
			return true

		},
		remove : (x) => {
			let n = hashCode(x)
			if( n<0 ){
				n = -n
			}
			n = n % buckets.length
			let current = buckets[n]
			let previous = undefined
			while(current!==undefined){
				if(equals(x,current.data)){
					if(previous===undefined){
						buckets[n] = current.next
					}
					else{
						previous.next = current.next
					}
					size--
					return true
				}
				previous=current
				current = current.next
			}
			return false
		},

		iterator : () => {

			let bucket = 0
			let previous = undefined
			let previousBucket = buckets.length
			let current = undefined
			while(bucket < buckets.length && buckets[bucket] === undefined){
				bucket++
			}
			if (bucket < buckets.length){
				current = buckets[bucket]
			}

			return {
				hasNext : () => {
					return current!==undefined

				},
				next : () => {
					let r = current.data
					if(current.next===undefined){
						previousBucket = bucket
						bucket++

						while(bucket < buckets.length && buckets[bucket].data === undefined){
							bucket++
						}

						if (bucket < buckets.length){
               			current = buckets[bucket];
               			r = current.data
               			current = current.next
               			}

               			else{
               				current=undefined
               			}
					}

					else{
						previous = current
						current = current.next
					}
					return r

				},
				remove : () => {
					if (previous !== undefined){
           				previous.next = previous.next.next;
           			}
         			else if (previousBucket < buckets.length){
            			buckets[previousBucket] = undefined;
         			}
         			else {
            			//throw new IllegalStateException();
         			}
         			previous = null;
        			previousBucket = buckets.length;

				},

			}

		},

		size : () => {
			return size
		}
	}
	//}
	/*catch{
	throw new Error({'Failure'})
	}*/
}

module.exports = { createHashSet }