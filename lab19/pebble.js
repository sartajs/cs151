class AbstractGame {
  winningMove(pos) {
    for (const n of this.move(pos)) {  
      if (this.losingPosition(n)) return n // Can force the opponent to lose
    }
    return undefined // Couldn't find a winning move
  }
  
  losingPosition(pos) {
    if (this.win(pos)) return true // Just lost
    for (const n of this.move(pos)) { 
      if (this.winningMove(n) === undefined) return false // Not losing here
    }
    return true // Lose no matter what
  }  
}
// NEXT SECTION
class PebbleGame extends AbstractGame {
  move(p) {
    if (p > 2) return [p - 2, p - 1]
    else if (p === 2) return [1]
    else return []
  }
  win(p) {
    return p === 1
  }
}
// NEXT SECTION

const myGame = new PebbleGame()

for (let p = 1; p <= 16; p++) {
  console.log(p + ' -> ' + myGame.winningMove(p))
}