import javax.swing.*;

/**
   A program for editing graphs.
 */
public class SimpleGraphEditor
{
   /**
    * main program
    * @param args main
    */
   public static void main(String[] args)
   {
      JFrame frame = new GraphFrame(new SimpleGraph());
      frame.setVisible(true);
   }
}

