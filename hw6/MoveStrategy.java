import java.util.List;
/*
 * interface for MoveStrategy
 */
public interface MoveStrategy {
	/**
	 * abstract declaration of movement process
	 * @param shapes the shapes to be moved
	 */
	void process(List<MoveableShape> shapes);
}
